package blas;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;

@SuppressWarnings("serial")
public class IntText extends JTextField implements KeyListener{
	
	//private Integer number;
	
	public IntText(int size) {
		super(size);
		this.addKeyListener(this);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		char c= e.getKeyChar();
		if(Character.isDigit(c) || c==KeyEvent.VK_DELETE || c==KeyEvent.VK_BACK_SPACE) {
			System.out.println("Number being typed in");
		}
		else {
			System.out.println("Not a Number");
			e.consume();
		}
	}
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {}

}
