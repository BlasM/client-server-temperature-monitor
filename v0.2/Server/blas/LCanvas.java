package blas;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.*;
import java.util.*;

@SuppressWarnings("serial")
public class LCanvas extends Canvas{
	
	//private int time, timeOld;
	//private int temp, tempOld;
	private Vector<Integer> valuesC1 = new Vector<Integer>(1);
	private Vector<Integer> valuesC2 = new Vector<Integer>(1);
	private Vector<Integer> valuesC3 = new Vector<Integer>(1);
	private Vector<Integer> valuesC4 = new Vector<Integer>(1);
	private Vector<Integer> valuesC5 = new Vector<Integer>(1);
	//private String clientName = "";
	private boolean c1,c2,c3,c4,c5;
	private int max,min;
	
	public LCanvas(int temp, int time) {
		this.setSize(new Dimension(400,200));
		this.setBackground(Color.white);
		//this.temp = temp;
		//this.time = time;
		/*this.client1 = new Color(100);
		this.client2 = new Color(200);
		this.client1 = new Color(50);
		*/
		//this.update(); //
	}
	
	//public void setTemp(Vector<Integer> v1, String name1,Vector<Integer> v2, String name2) {
	public void setTemp(Vector<Integer> v1, Vector<Integer> v2, Vector<Integer> v3, Vector<Integer> v4, Vector<Integer> v5, boolean c1, boolean c2, boolean c3, boolean c4, boolean c5, int max,int min) {
		 //values.addElement(new Point(x,y) );
			this.valuesC1 = v1;
			this.valuesC2 = v2;
			this.valuesC3 = v3;
			this.valuesC4 = v4;
			this.valuesC5 = v5;
			this.c1 = c1;
			this.c2 = c2;
			this.c3 = c3;
			this.c4 = c4;
			this.c5 = c5;
			this.max = max;
			this.min = min;
		this.repaint();
	}
		
	public void paint (Graphics g) {
		//Axis for the plots
		g.setColor(Color.BLACK);
		g.drawLine(10, 0, 10, this.getHeight());	// Y Axis
		g.drawLine(0,this.getHeight()-10, this.getWidth(), this.getHeight()-10);	// X-axis
		
		//PLoting of Client 1
		if (c1) {							//(clientName.contentEquals("client1")) { // it does not like
			g.setColor(Color.red);
			//System.out.println("Vector values " + valuesC1);
			//System.out.println("Vector size " + valuesC1.size());
			for(int a = 1 ; a<valuesC1.size() ; a++) {
				int y= this.valuesC1.elementAt(a);
				int yOld= this.valuesC1.elementAt(a-1);
				g.drawLine(((a-1)*20),(this.getHeight()-yOld),(a*20),(this.getHeight()-y));
			}
		}
		//Ploting Client 2
		if(c2) {
			g.setColor(Color.green);
			for(int a = 1 ; a<valuesC2.size() ; a++) {
				int y= this.valuesC2.elementAt(a);
				int yOld= this.valuesC2.elementAt(a-1);
				g.drawLine(((a-1)*20),(this.getHeight()-yOld),(a*20),(this.getHeight()-y));
			}
		}
		//Ploting Client 3
		if(c3) {
			g.setColor(Color.blue);
			for(int a = 1 ; a<valuesC3.size() ; a++) {
				int y= this.valuesC3.elementAt(a);
				int yOld= this.valuesC3.elementAt(a-1);
				g.drawLine(((a-1)*20),(this.getHeight()-yOld),(a*20),(this.getHeight()-y));
			}
		}
		//Ploting Client 4
		if(c4) {
			g.setColor(Color.magenta);
			for(int a = 1 ; a<valuesC4.size() ; a++) {
				int y= this.valuesC4.elementAt(a);
				int yOld= this.valuesC4.elementAt(a-1);
				g.drawLine(((a-1)*20),(this.getHeight()-yOld),(a*20),(this.getHeight()-y));
			}
		}
		//Ploting Client 5
		if(c5) {
			g.setColor(Color.yellow);
			for(int a = 1 ; a<valuesC5.size() ; a++) {
				int y= this.valuesC5.elementAt(a);
				int yOld= this.valuesC5.elementAt(a-1);
				g.drawLine(((a-1)*20),(this.getHeight()-yOld),(a*20),(this.getHeight()-y));
			}
		}
		g.setColor(Color.gray);//Color.lightGray);
		g.drawLine(0,(this.getHeight()-max), this.getWidth(), (this.getHeight()-max));	//Max value delimiter
		g.drawString("Max_val",this.getWidth()/2,(this.getHeight()-max));
		g.drawLine(0,(this.getHeight()-min), this.getWidth(), (this.getHeight()-min));	//Min value delimiter
		g.drawString("Min_val",this.getWidth()/2,(this.getHeight()-min));
	}

}
