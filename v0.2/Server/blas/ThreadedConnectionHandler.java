/* The Connection Handler Class - Written by Derek Molloy for the EE402 Module
 * See: ee402.eeng.dcu.ie
 */

package blas;

import java.net.*;
import java.io.*;

public class ThreadedConnectionHandler extends Thread
{
    private Socket clientSocket = null;				// Client socket object
    private ObjectInputStream is = null;			// Input stream
    private ObjectOutputStream os = null;			// Output stream
    //private TemperatureService theTemp;
    private int temp;
    private GuiServer gui;
    private int sRate = 5000;
    private String clientName;


    
	// The constructor for the connection handler
    public ThreadedConnectionHandler(Socket clientSocket, GuiServer gui) {
        this.clientSocket = clientSocket;
        this.gui = gui;
        //Set up an object to get the Temperature 
        //theTemp = new TemperatureService();
    }
    public int getTemp() {
    	return(temp);
    }

    // Will eventually be the thread execution method - can't pass the exception back
    public void run() {
    	//int counter = 0;
        gui.countUpClient();

	         try {
	            this.is = new ObjectInputStream(clientSocket.getInputStream());
	            this.os = new ObjectOutputStream(clientSocket.getOutputStream());
	            System.out.println("Connection Handler starting ....");
	
	            while (this.readCommand()) {
					//counter++;
	            	//System.out.println("**** Communication Established ****");
	            	//System.out.println("** Enable signal value " + GuiServer.start_en.toString() + " ***");
	            	//System.out.println("Data present at the ConnectionHandler" + this.gui);
					//System.out.println("Data to be populated by ConnectionHandler" + this.gui.getName());
				}
	         } 
	         catch (IOException e) 
	         {
	        	System.out.println("XX. There was a problem with the Input/Output Communication:");
	            e.printStackTrace();
	         }
    }

    // Receive and process incoming string commands from client socket 
    private boolean readCommand() {
		TemperatureService s = new TemperatureService();
        try {
			s = (TemperatureService) is.readObject();
            System.out.println("Reading an Object from the Client");
        } 
        catch (Exception e){    // catch a general exception
        	System.out.println("Closing Socket for the Client");
        	gui.countDownClient();
			this.closeSocket();
            return false;
        }
        //System.out.println("01. <- Received an Object from the client (" + s + ").");
        
        this.updateGraph(s);
		
		this.updateInfo();
		
		
        return true;
    }
    
    private void updateGraph(TemperatureService s) {	//synchronsed method??
    	// Client name
    	clientName = s.getName();
		System.out.println("[ConnHandler]-- From the client: " + clientName);
		// Sample number
    	int numb = s.getnSample();
    	System.out.println("[ConnHandler] -- Sample received received is " + numb);
    	// Temp value
    	int Temp = (int)s.getTemp();
    	System.out.println("Temperature received is " + Temp);
    	gui.setDrawVal(Temp, numb,clientName);
    }
    
    private void updateInfo() { 
    	TemperatureService a = new TemperatureService(sRate,false);
    	if(gui.getStime() != sRate) {
    		sRate = gui.getStime();
			a.setSampling(sRate);
			System.out.println("Updating Sampling Rate for the Clients to " + sRate);
    	}
    	if(gui.getAlert()!=0) {
    		if( clientName.equals(gui.getAlertName())){
    			a.setShutdown();
    		}    		
    	}
        this.send(a);
    }

    // Send a generic object back to the client 
    private void send(Object o) {
        try {
            //System.out.println("02. -> Sending (" + o +") to the client.");
            this.os.writeObject(o);
            this.os.flush();
        } 
        catch (Exception e) {
            System.out.println("XX." + e.getStackTrace());
        }
    }
    
    // Send a pre-formatted error message to the client 
    public void sendError(String message) { 
        this.send("Error:" + message);	//remember a String IS-A Object!
    }
    
    // Close the client socket 
    public void closeSocket() { //gracefully close the socket connection
        try {
            this.os.close();
            this.is.close();
            this.clientSocket.close();
        } 
        catch (Exception e) {
            System.out.println("XX. " + e.getStackTrace());
        }
    }
}