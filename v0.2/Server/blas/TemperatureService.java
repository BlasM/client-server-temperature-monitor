/* The Temperature Service Class - Written by Blas Molina for the EE402 Module
 * 
 */
package blas;

import java.io.Serializable;
import java.util.Date;

public class TemperatureService implements Serializable
{
	
	private String client_name;				//accepted names {client[1-5]}
	private int client_temp;
	private int client_sampling;			//expected values {5000: 5sec; 10000: 10 sec; 20000: 20 sec}
	private int client_nSample;
	private boolean client_shutdown;
	private String client_date;
	
	public TemperatureService() {
		this.client_name = null;
		this.client_temp = 0;
		this.client_sampling = 5000;
		this.client_nSample = 0;
		this.client_shutdown = false;
		this.client_date = null;
	}	
	public TemperatureService(String client_name, int client_temp) {
		this.client_name = client_name;
		this.client_temp = client_temp;
		this.client_sampling = 5000;
		this.client_nSample = 0;
		this.client_shutdown = false;
		}
	public TemperatureService(int client_sampling, boolean client_shutdown) {
		this.client_sampling = client_sampling;
		this.client_shutdown = client_shutdown;
	}
	public TemperatureService(String name, int temp, int sampling, int nSample, String date) {
		this.client_name = name;
		this.client_temp = temp;
		this.client_sampling = sampling;
		this.client_nSample = nSample;
		this.client_shutdown = false;
		this.client_date = date;
	}
	
	// Get and Set Functions //	
	public String getName() {
		return this.client_name;
	}
	public int getTemp() {
		return this.client_temp;
	}
	public boolean getShutdown() {
		return this.client_shutdown;
	}
	public int getStime() {
		return this.client_sampling;
	}
	public int getnSample() {
		return this.client_nSample;
	}
	public void setTemp(int temp) {
		this.client_temp = temp;
	}
	public void setSampling(int sampling) {
		this.client_sampling = sampling;
	}
	public void setnSample(int nSample) {
		this.client_nSample = nSample;
	}
	public void setShutdown() {
		this.client_shutdown = true;
	}
}
