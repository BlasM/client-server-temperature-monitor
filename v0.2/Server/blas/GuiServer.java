package blas;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.*;

@SuppressWarnings("serial")
public class GuiServer extends JFrame implements ActionListener, ListDataListener, WindowListener{
	
	private JButton start_button;
	private JRadioButton sp_button2,sp_button5,sp_button10,sp_button20;
	private IntText maxTemp_int;
	private JTextField nClients_field;
	private JLabel maxTemp_label,nClients_label;
	private JPanel sp_ratePanel,cb_panel,plot_panel,features_panel,maxTemp_panel,nClients_panel,panel_canvas;
	//private DefaultListModel sp_rateNames;
	private JList<String> sp_rates;
	private LCanvas sq_canvas;
	private JCheckBox client1,client2,client3,client4,client5,maxTemp_en;
	static Boolean start_en = false;
	private int sample = 0,sRate=5000,maxVal=0,minVal=100;
	volatile private int connected_clients = 0;
	private int temp ,maxTemp_val = 100, alert = 0;
	private Thread theServer;
	//private ThreadedServer myServer;
	private String cName1,cName2,cName3,cName4,cName5;// = "default";
	private boolean c1 = true,c2 = true,c3=true,c4=true,c5 = true;
	private String name_alert="NoName";
	//private Vector<String> vClients = new Vector<String>(5);
	
	private Vector<Integer> vTemp1 = new Vector<Integer>(1);
	private Vector<Integer> vTemp2 = new Vector<Integer>(1);
	private Vector<Integer> vTemp3 = new Vector<Integer>(1);
	private Vector<Integer> vTemp4 = new Vector<Integer>(1);
	private Vector<Integer> vTemp5 = new Vector<Integer>(1);

	
	/**
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public GuiServer() {
		super("PC GUI Server");
		// Create a Pane to contain the elements
		this.setLayout(new BorderLayout());
		
		//Sampling Time Radio buttons List
			// Label for the Sampling TIme ratio
	     	JLabel sp_label = new JLabel("Sampling Time",JLabel.CENTER);
	      
	      // Button Test for SP rate sub-panel
	      start_button = new JButton("Start/Stop");
	      start_button.addActionListener(this);
	      
	      //Temp empty panel
	      JPanel sp_rates = new JPanel(new GridLayout(3,1));
	      JRadioButton sp_button2 = new JRadioButton("2 Sec",false);
	      sp_button2.addActionListener(this);
	      JRadioButton sp_button5 = new JRadioButton("5 Sec",true);
	      sp_button5.addActionListener(this);
	      JRadioButton sp_button10 = new JRadioButton("10 Sec",false);
	      sp_button10.addActionListener(this);
	      JRadioButton sp_button20 = new JRadioButton("20 Sec",false);
	      sp_button20.addActionListener(this);
	      ButtonGroup sp_group = new ButtonGroup();
	      sp_group.add(sp_button2);
	      sp_group.add(sp_button5);
	      sp_group.add(sp_button10);
	      sp_group.add(sp_button20);
	      
	      sp_rates.add(sp_button2);
	      sp_rates.add(sp_button5);
	      sp_rates.add(sp_button10);
	      sp_rates.add(sp_button20);
	      
	      // Sampling Time subPanel
	      sp_ratePanel = new JPanel(new BorderLayout());
	      sp_ratePanel.add(start_button,BorderLayout.SOUTH);
	      sp_ratePanel.add(sp_rates,BorderLayout.CENTER);
	      sp_ratePanel.add(sp_label,BorderLayout.NORTH);
	      
	      // Graph panel with the Drawing/Plot and the Client buttons at the bottom
	      plot_panel = new JPanel(new BorderLayout());
		      //Internal Panel with the buttons for Clients
			  cb_panel = new JPanel(new FlowLayout());
		      // Check Box buttons 
		      client1 = new JCheckBox("Client 1",true);
		      client1.addActionListener(this);
		      client2 = new JCheckBox("Client 2",true);
		      client2.addActionListener(this);
		      client3 = new JCheckBox("Client 3",true);
		      client3.addActionListener(this);
		      client4 = new JCheckBox("Client 4",true);
		      client4.addActionListener(this);
		      client5 = new JCheckBox("Client 5",true);
		      client5.addActionListener(this);
			  //Add Client buttons to the panel 
			  cb_panel.add(client1);
			  cb_panel.add(client2);
			  cb_panel.add(client3);
			  cb_panel.add(client4);
			  cb_panel.add(client5);	      
			  // Graph panel with the drawing of the temperatures and the labels
			  Label temp = new Label("Temperature (C)");
			  Label samples = new Label("Number of Samples");
			  sq_canvas = new LCanvas(250,250);
			  panel_canvas = new JPanel( new BorderLayout());
			  panel_canvas.add(temp,BorderLayout.WEST);
			  panel_canvas.add(samples,BorderLayout.SOUTH);
			  panel_canvas.add(sq_canvas,BorderLayout.CENTER);
		  //Adding components to the Drawing/Plot panel
		  plot_panel.add(cb_panel,BorderLayout.SOUTH);
		  plot_panel.add(panel_canvas,BorderLayout.CENTER);
		   
		  //Extra features Panel : Max Temp set & Alert
		  features_panel = new JPanel(new BorderLayout());
		  //Max Temp panel with Alarm
		  maxTemp_panel = new JPanel(new FlowLayout());
		  //Label for the Features
		  maxTemp_label = new JLabel("CPU Max Temp");
		  maxTemp_label.setHorizontalAlignment(JLabel.CENTER);
		  maxTemp_label.setVerticalAlignment(JLabel.CENTER);
		  
		  // Buttons and field for Max Temp Alarm
		  maxTemp_en = new JCheckBox("Max Temperature Alarm");
		  maxTemp_en.addActionListener(this);
		  maxTemp_int = new IntText(3);
		  maxTemp_int.addActionListener(this);
		  
	      // Add objects to the Max Temp Alarm panel
	      maxTemp_panel.add(maxTemp_en);
	      maxTemp_panel.add(maxTemp_int);

	      // Number of Clients Panel
	      nClients_panel = new JPanel();
	      nClients_label = new JLabel ("Number of Clients connected");
	      nClients_field = new JTextField(3);
	      nClients_field.setForeground(Color.green);
	      nClients_field.setMargin(new Insets(0,1,0,1));
	      //nClients_field.setEditable(false);
	      nClients_panel.add(nClients_label);//,BorderLayout.CENTER);
	      nClients_panel.add(nClients_field); //,BorderLayout.EAST);
	      // Add two sub-panels to the features Panel
	      features_panel.add(maxTemp_label,BorderLayout.NORTH);
	      features_panel.add(maxTemp_panel,BorderLayout.CENTER);
	      
	    //Main Panel
	    this.getContentPane().add("West",sp_ratePanel); //,BorderLayout.WEST);
	    this.getContentPane().add("Center",plot_panel); //,BorderLayout.CENTER);
	    this.getContentPane().add("South",features_panel); //,BorderLayout.SOUTH);
	    this.getContentPane().add("North",nClients_panel);
	    	    
	    // Enable main panel and set size
		this.pack();
		this.setLocationRelativeTo(null);
		this.setSize(750, 400);
		this.setVisible(true);
		//Panel actions
		this.addWindowListener(this);
		
	}
	public int getStime() {
		return(this.sRate);
	}
	/*public void addClient(String e) {
		this.vClients.add(e);
	} 
	public void remClient(String e) {
			if (this.vClients.remove(e)) {
				System.out.println("*** Client : " + e + " was removed ****");
			}
	}
	public int checkClient(String e) {
		 //int a =  this.vClients.size();
		 int res = 0;
		 for (int i=0; i<this.vClients.size();i++) {
			 if(this.vClients.get(i).equals(e)) {
			 	res = i;
			 } else {res = 0;}
		 }
		 return res;
	}
	*/
	// Extra features Methods //
	public int getAlert() {
		//System.out.println("Alert value for Shuting down Client: " + alert);
		return this.alert; 
	}
	
	public synchronized void countUpClient () {
		this.connected_clients +=1;
		//this.nClients_field.setText(connected_clients.toString());
		this.nClients_field.setText(Integer.toString(connected_clients)); 
	}
	public synchronized void countDownClient () {
		this.connected_clients -=1;
		//this.nClients_field.setText(connected_clients.toString());
		this.nClients_field.setText(Integer.toString(connected_clients)); 
	}
	
	public String getAlertName() {
		System.out.println("[GUI Server] Alert value for Shuting down Client: " + name_alert);
		return this.name_alert; 
	}
	
	// Private method which updates the Canvas //
	private void update() {
		//this.text.setText("Current count from Bar is: " + this.sb.getValue());
		
		//this.sq_canvas.setTemp(vTemp); // calls the method from Square Canvas  to update the size 
		this.sq_canvas.setTemp(vTemp1,vTemp2,vTemp3,vTemp4,vTemp5,c1,c2,c3,c4,c5,maxVal,minVal);	
	// *** Listening function for the Max Temp field **** //
	}
	
	// *** Buttons and Text fields events ***//
	public void actionPerformed(ActionEvent e) {
		
		// Start/Stop Client
		if (e.getActionCommand().equals("Start/Stop")) {
			start_en = !start_en;

			//this.update();
			if (start_en == true) {
				System.out.println("Starting Server");
				//vClients.add(new String(""));  // Initialize Vector Names when the Server starts
				//theServer = new Thread(myServer(this));
				theServer = new Thread(new ThreadedServer(this));
				theServer.start();
			}
			else if (start_en == false) {
				System.out.println("Stoping Server");
				//theServer.togServer();
			}			
		}			
		// Sampling Radix Buttons
		if(e.getActionCommand().equals("2 Sec")) {
			System.out.println("[2 seconds sampling rate choosen]");
			sRate = 2*1000;
		}else if(e.getActionCommand().equals("5 Sec")) {
			System.out.println("[5 seconds sampling rate choosen]");
			sRate = 5*1000;
		}
		else if (e.getActionCommand().equals("10 Sec")) {
			System.out.println("[10 seconds sampling rate choosen]");
			sRate = 10 *1000;
		}
		else if (e.getActionCommand().equals("20 Sec")) {
			System.out.println("[20 seconds sampling rate choosen]");
			sRate = 20*1000;
		}		
		// Client enable buttons
		if (e.getSource().equals(client1)) {
			if (client1.isSelected() == true) {
			System.out.println("Client 1 Enabled");
			c1 = true;
			}
			else if (client1.isSelected() == false) {
			System.out.println("Client 1 Disabled");
			c1=false;
			}
		}
		else if (e.getSource().equals(client2)) {
			if (client2.isSelected() == true) {
			System.out.println("Client 2 Enabled");
			c2 = true;
			}
			else if (client2.isSelected() == false) {
			System.out.println("Client 2 Disabled");
			c2 = false;
			}
		}
		else if (e.getSource().equals(client3)) {
			if (client3.isSelected() == true) {
			System.out.println("Client 3 Enabled");
			c3 = true;
			}
			else if (client3.isSelected() == false) {
			System.out.println("Client 3 DISabled");
			c3 = false;
			}
		}
		else if (e.getSource().equals(client4)) {
			if (client4.isSelected() == true) {
			System.out.println("Client 4 Enabled");
			c4 = true;
			}
			else if (client4.isSelected() == false) {
			System.out.println("Client 4 DISabled");
			c4 = false;
			}
		}
		else if (e.getSource().equals(client5)) {
			if (client5.isSelected() == true) {
			System.out.println("Client 5 Enabled");
			c5 = true;
			}
			else if (client5.isSelected() == false) {
			System.out.println("Client 5 DISabled");
			c5 = false;
			}
		}
		
		// Max Temp Button
		if (e.getSource().equals(maxTemp_en)) {
			if (maxTemp_en.isSelected() == true) {
				System.out.println("Max Temp Enabled");
				this.maxTemp_int.setText("100");
				}
				else if (maxTemp_en.isSelected() == false) {
				System.out.println("Max Temp Disabled");
				this.maxTemp_int.setText("");
				}
			}
		if (e.getSource().equals(maxTemp_int)) {	//Getting the Max value text2int
			//String text = maxTemp_int.getText();
			try {
				if (maxTemp_en.isSelected()==true) {
					maxTemp_val = Integer.valueOf(maxTemp_int.getText());
				}
			} catch (NumberFormatException a) {
				System.out.println("Max value could not be casted into Integer type " + a.getMessage());
			}

		}
	}
	
	// Method to pass the Valu from the Connector Handler //
	public void setDrawVal(int a, int b, String name) {
		this.temp = a;
		this.sample = b%20;	// Limits the sample to values [0-20]
		//System.out.println("[Values vector] Vector location: " + sample);
		System.out.println("[GUI Server] Updating Vectors values for " + name + " before sending to plot");
		
		// Check CLients name and add them to the list 
		/*if (checkClient(name) == 0) {
			addClient(name);
			connected_clients = vClients.size();
		}
		*/
		
		//Check for Temp Max value and thorughs an pop-up window 
		if(temp >= maxTemp_val) {
			alert = JOptionPane.showConfirmDialog(null,"Do you want to Shutdown Client: " + name + " ?", "Alert of Max Temp",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
			alert ^= 1;	//Turn alert into (1)  for "Yes" an (0) for "No" shuting down the server
			name_alert = name;
		}
		
		//checks for the Max and min Temperature values
		if (temp > maxVal) {this.maxVal = temp;}
		if (temp < minVal) {this.minVal = temp;}
		
		if (name.equals("client1")) {
			//this.vTemp1.add(sample,temp);
			updateVect(vTemp1);
			/*System.out.println("Vector size C1 is: " + vTemp1.size());
			if (vTemp1.size() == 20) {
				vTemp1.remove(0);
				System.out.println("Element removed!! vector size is "+ vTemp1.size());
			} 
			vTemp1.add(temp); // Always add elements
*/			this.cName1 = name;
		} else if (name.contentEquals("client2")) {
			updateVect(vTemp2);
			this.cName2 = name;
		} else if (name.contentEquals("client3")) {
			updateVect(vTemp3);
			this.cName3 = name;
		} else if (name.contentEquals("client4")) {
			updateVect(vTemp4);
			this.cName4 = name;
		} else if (name.contentEquals("client5")) {
			updateVect(vTemp5);
			this.cName5 = name;
		}
		
		// moving average
		// Need to short out how to calculate average with the nul vectors. Check for the contect before adding them 
		//avg = (vTemp1.lastElement() + vTemp2.lastElement() + vTemp3.lastElement() + vTemp4.lastElement() + vTemp5.lastElement())/connected_clients;
		//vTemp.add((sample),temp);
		
		// Pass the values to the Canvas
		this.update();
	}
	// Keeps the Vector up to 20-samples while it keeps adding samples to the end and removing the oldest one
	private void updateVect (Vector<Integer> a) {
		if (a.size() == 20) {
			a.remove(0);
			//System.out.println("Element removed!! vector size is "+ a.size());
		} 
		a.add(temp);
	}
	
	
	// **** Abstract methods to handle PC GUI Server Window performance: open,close,minimize 
	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void	windowClosing(WindowEvent arg0) {
		System.exit(0);
	} 
	public void	windowDeactivated(WindowEvent arg0) {} 
	public void	windowDeiconified(WindowEvent arg0) {} 
	public void	windowIconified(WindowEvent arg0) {} 
	public void	windowOpened(WindowEvent arg0) {} 
	
	// **** Absract methods to handle the JList with performance not necessary here but required
	// to be implemented 
	public void intervalAdded(ListDataEvent e) {}
	public void intervalRemoved(ListDataEvent e) {}
	public void contentsChanged(ListDataEvent e) {}
	
	// **** Main function ***** //
	public static void main(String[] args) {
		new GuiServer();
		
	}

	
}
