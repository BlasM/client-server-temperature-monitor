/* The Client Class - Written by Derek Molloy for the EE402 Module
 * See: ee402.eeng.dcu.ie
 * modified by Blas Molina for Client-Server Application Assigment 
 * 
 */

package blas;

import java.net.*;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.io.*;

public class Client {
	
	private int portNumber;
    private Socket socket = null;
    private ObjectOutputStream os = null;
    private ObjectInputStream is = null;
    private TemperatureService objTemp;
    private DateTimeService objTime;
    private String cName;
    private Scanner fileTemp;
    private boolean shutdown = false;
    private int timer = 5000;		//Time in miliseconds
    private int intCounter = 0;
    private String LED0_PATH = "/sys/class/leds/led0"; 
    private String TEMP_PATH ="C:\\Users\\paria\\EE402_OOP\\JAVA\\Client\\src\\blas\\temp"; //RaspBerryPI code: "/sys/class/thermal/thermal_zone0/temp";

	// the constructor expects the IP address of the server - the port is fixed
    public Client(String serverIP, int serverSocket,String clientName) {
    	this.cName = clientName;
    	this.portNumber = serverSocket;
    	if (!connectToServer(serverIP)) {
    		System.out.println("XX. Failed to open socket connection to: " + serverIP);            
    	}
    }

    private boolean connectToServer(String serverIP) {
    	try { // open a new socket to the server 
    		this.socket = new Socket(serverIP,portNumber);
    		this.os = new ObjectOutputStream(this.socket.getOutputStream());
    		this.is = new ObjectInputStream(this.socket.getInputStream());
    		System.out.println("00. -> Connected to Server:" + this.socket.getInetAddress() 
    				+ " on port: " + this.socket.getPort());
    		System.out.println("    -> from local address: " + this.socket.getLocalAddress() 
    				+ " and port: " + this.socket.getLocalPort());
    	} 
        catch (Exception e) {
        	System.out.println("XX. Failed to Connect to the Server at port: " + portNumber);
        	System.out.println("    Exception: " + e.toString());	
        	return false;
        }
		return true;
    }

    private void info() {
    	while(!shutdown) {
	    	// Create TmeperatureService Object to be sent
	    	int localTemp = calculateTemperature();
	    	objTime = new DateTimeService();
	    	String ldate = objTime.getDateAndTime();
	    	objTemp = new TemperatureService(this.cName, localTemp,timer,intCounter,ldate);
	    	// SendingObject
	    	System.out.println("01. -> " + "[" + ldate + "]"+  " with Temperature " + localTemp + " (C) and Sampling Time: " + (timer/1000) +  "s to the server...");
	    	flashLed();
	    	this.send(objTemp);
	    	
	    	//Waiting to receive an Object
	    	try{
	    		objTemp = (TemperatureService) receive();
	    		System.out.println("05. <- The Server responded with: ");
	    		System.out.println("    <- " + objTemp);
	    		intCounter+=1;
	    		updateParam(objTemp);
	    	}
	    	catch (Exception e){
	    		System.out.println("XX. There was an invalid object sent back from the server");
	    	}
	    	try{
	    		if(shutdown) {
	    			System.out.println("06. -- Disconnected from Server.");
	    			System.exit(0);
	    		}
	    		else {
			    	System.out.println("06. -- Waiting for to send next Sample after Sampling-Time: " + (timer/1000) + " sec");
		    		Thread.sleep(timer);
	    		}
	    	} 
	    	catch(InterruptedException e){
	    		System.out.println("Thread was Interrupted!"); } 
	    	
    	}
    	System.out.println("06. -- Disconnected from Server.");
    }
	
    // method to send a generic object.
    private void send(Object o) {
		try { 
		    System.out.println("02. -> Sending an object...");
		    os.writeObject(o);
		    os.flush();
		} 
	    catch (Exception e) {
		    System.out.println("XX. Exception Occurred on Sending:" +  e.toString());
		}
    }

    // method to receive a generic object.
    private Object receive() 
    {
		Object o = null;
		try {
			System.out.println("03. -- About to receive an object...");
		    o = is.readObject();
		    System.out.println("04. <- Object received...");
		} 
	    catch (Exception e) {
		    System.out.println("XX. Exception Occurred on Receiving:" + e.toString());
		}
		return o;
    }
    public int calculateTemperature() {
		int tmp_val = 0;
		try { 
			fileTemp = new Scanner(new File(TEMP_PATH));
		}
		catch (FileNotFoundException a) {
			System.out.println("XX. There was a problem reading the file");
            a.printStackTrace();
		}
		try {
			while(fileTemp.hasNextInt())
			{
				tmp_val = fileTemp.nextInt();
				System.out.println("Value read from the file: " + tmp_val);
			}
		}
		catch (NoSuchElementException a) {
			System.out.println("XX. File format not accepted");
            a.printStackTrace();			
		}				
		/* Divided by 10^3 to convert Temperture to Degs &
		 * add some random value to the temperature up to 30 degrees difference 
		 * & round the value to 2 decimal digits
		 */
		tmp_val = (int)(Math.round(tmp_val/1000))+ (int)(Math.random() * 30);//Math.round(((tmp_val/1000)+ (Math.random() * 10))*100.0)/100.0; //Math.round(tmp_val * 1000.0)/100.0;
		System.out.println("Calcualtions done for " + cName + " with " + tmp_val);
		
		if (fileTemp != null)
			fileTemp.close();
		
		return (tmp_val);
	}
    
    public void updateParam(TemperatureService a) {
    	if (a.getShutdown()== true) {
    		shutdown = a.getShutdown();
    		System.out.println("*** [Updating Shutdown command to TRUE] ***");
    	}
    	if (a.getStime()!= timer) {
    		timer = a.getStime();
    		System.out.println("[Updating Timer to " + (timer/1000) + " sec]");
    	}    	
    }
    
    public void flashLed() {
    	try{
    		BufferedWriter bw = new BufferedWriter ( new FileWriter (LED0_PATH+"/trigger"));
			bw.write("none");
			bw.close();
			bw = new BufferedWriter ( new FileWriter (LED0_PATH+"/brightness"));
			bw.write("1");
			Thread.sleep(500);
			bw.write("0");
			bw.close();
		}
		catch(IOException e){
			System.out.println("Failed to access the RaspBerryPI  LEDs"+ e.toString());
		}
		catch(InterruptedException e){
	    		System.out.println("Failed Flashig LED :( "); }						
    	
    }
    public static void main(String args[]) 
    {
    	System.out.println("**. Java Client Application - EE402 OOP Module, DCU");
    	System.out.println("**. With the format: <Host_name> <Socket_number> <Client_name>");
     	if(args.length==3){
    		int socketNum = Integer.parseInt(args[1]);
    		Client theApp = new Client(args[0], socketNum, args[2]);
		    theApp.info();
		}
    	else
    	{
    		System.out.println("Error: you must provide the address of the server, the Socket port number and the Client name");
    		System.out.println("Usage is:  java Client x.x.x.x [5050] client[1-5] (e.g. java Client 192.168.7.2 5050 client1)");
    		System.out.println("      or:  java Client hostname (e.g. java Client localhost 5050 client2)");
    	}    
    	System.out.println("**. End of Application.");
    }
}