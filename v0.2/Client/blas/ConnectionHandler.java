/* The Connection Handler Class - Written by Derek Molloy for the EE402 Module
 * See: ee402.eeng.dcu.ie
 */

//package ee402;
package blas;


import java.net.*;
import java.io.*;

public class ConnectionHandler
{
    private Socket clientSocket = null;				// Client socket object
    private ObjectOutputStream os = null;			// Output stream
    private ObjectInputStream is = null;			// Input stream
    private TemperatureService theTemp;

    
	// The constructor for the connection handler
    public ConnectionHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
        //Set up a service object to store the Temperature
        theTemp = new TemperatureService();
    }
    // Will eventually be the thread execution method - can't pass the exception back
    public void init() {
    	int counter = 0;
         try {
            this.os = new ObjectOutputStream(clientSocket.getOutputStream());
            this.is = new ObjectInputStream(clientSocket.getInputStream());
            System.out.println("Connection Handler working");
            while (this.readCommand()) { 
            	counter++;
            	System.out.println("Counter value " + counter);
            }
         } 
         catch (IOException e) 
         {
        	System.out.println("XX. There was a problem with the Input/Output Communication:");
            e.printStackTrace();
         }
    }

    // Receive and process incoming string commands from client socket 
    private boolean readCommand() {
        TemperatureService s = new TemperatureService();
        System.out.println("Reading Command!!");
        try {
            s = (TemperatureService) is.readObject();
            System.out.println("Reading the Object from the Client");
        } 
        catch (Exception e){    // catch a general exception
        	this.closeSocket();
            System.out.println("Closing Socket for the Client");
            e.printStackTrace();
            return false;
        }
        System.out.println("01. <- Received an Object from the client (" + s + ").");
        
        // At this point there is a valid String object
        // invoke the appropriate function based on the command 
        
        /*if (s.equalsIgnoreCase("GetDate")){ 
            this.getDate(); 
        }*/
       // if (s.getName().equals("myclient")){ 
            //this.getDate();
        	System.out.println("Temperature Service Class Object received and readed!!");
        	System.out.println("Temperature received is " + s.getTemp());
        	System.out.println("Name received: " + s.getName());
        	System.out.println("Sample number received: " + s.getnSample());
        	System.out.println("Timer received: " + s.getStime());

        	boolean shutdownLocal = false;
        	if (s.getnSample() == 10) {
        		shutdownLocal = true;
            	System.out.println("Shudown command updated!!, closing CLient ");
        	}
        	//s.setSampling(2); Sets timer to 2 sec
        	this.updateInfo(shutdownLocal);
        //}    
        //else { 
        //    this.sendError("Invalid command: " + s); 
        //}
        return true;
    }

    // Use our custom DateTimeService Class to get the date and time
    private void updateInfo(boolean b) {	// use the date service to get the date
        //String currentDateTimeText = theDateService.getDateAndTime();
        //this.send(currentDateTimeText);
    	TemperatureService a = new TemperatureService(10000,b);
    	System.out.println("Updating values for the Client");
    	//a.setSampling(10);
        this.send(a);

    }

    // Send a generic object back to the client 
    private void send(Object o) {
        try {
            System.out.println("02. -> Sending (" + o +") to the client.");
            this.os.writeObject(o);
            this.os.flush();
        } 
        catch (Exception e) {
            System.out.println("XX." + e.getStackTrace());
        }
    }
    
    // Send a pre-formatted error message to the client 
    public void sendError(String message) { 
        this.send("Error:" + message);	//remember a String IS-A Object!
    }
    
    // Close the client socket 
    public void closeSocket() { //gracefully close the socket connection
        try {
            this.os.close();
            this.is.close();
            this.clientSocket.close();
        } 
        catch (Exception e) {
            System.out.println("XX. " + e.getStackTrace());
        }
    }
}